install.packages('ggplot2')
install.packages('reshape2')
install.packages("data.table")
install.packages("dplyr")
install.packages("stringr")
install.packages("graph")
install.packages("DT")
install.packages("reshape")
source("http://bioconductor.org/biocLite.R"); biocLite(c("graph", "RBGL"))
install.packages("Vennerable", repos="http://R-Forge.R-project.org")
