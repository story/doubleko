.libPaths("/home/shiny/rlibs")

library(shiny)
library(DT)
library(ggplot2)
# Load the ggplot2 package which provides

df.l <- readRDS("./all_dge_list.RDS")

##df.ul <- do.call(rbind,df.l)
df.ul <- df.l[[1]]

## Define the overall UI
shinyUI(
  fluidPage(
      titlePanel("DGE RNA-Seq DataTable - Mouse RNA-Seq NGLY1 and Eng Knockout with MG treatment"),
      ## Create a new Row in the UI for selectInputs
      fluidRow(
          column(4,
                 selectInput("sample",
                             "Comparison to Investigate:",
                             names(df.l))
                 ),
          column(4,
                 selectInput("biotype",
                             "Filter Gene Biotype:",
                             c("All",unique(as.character(df.ul$biotype))))
                 ),
          column(4,
                 selectInput("type",
                             "Type of Differential Expression:",
                             c("All",unique(as.character(df.ul$type))))
                 )

      ),
      ## Create a new row for the table.
      fluidRow(
          downloadButton('downloadData', 'Download'),
          h3(textOutput("title"),align='center',style="color:red"),
          htmlOutput("info")
      ),
      fluidRow(
          DT::dataTableOutput("table"),
          ##verbatimTextOutput("tabler"),
          column(8, align="center",
                 plotOutput("tablerplot",width='100%',height='720px')
                 )
      )
  )
)

