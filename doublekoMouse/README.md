# Image for Ticket/View/154154/


Install following packages.

 install.packages('ggplot2')
 install.packages('reshape2')
 install.packages("data.table")
 install.packages("dplyr")
 install.packages("stringr")

# Build image

Once you have imported the repo, you should change this build instructions

```
docker build -t git.embl.de:4567/moscardo/doubleko:v2 
```

# Push image

```
docker push git.embl.de:4567/moscardo/doubleko:v2
```
